// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "TopDownBase50Character.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/PlayerController.h"
#include "TopDownBase50PlayerController.generated.h"

class UWeaponComponent;
class ATopDownBase50Character;
/** Forward declaration to improve compiling times */
class UNiagaraSystem;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRightClick, FVector, LocationOfClick);

UCLASS()
class ATopDownBase50PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATopDownBase50PlayerController();

	/** Time Threshold to know if it was a short press */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;

	/** FX Class that we will spawn when clicking */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;

	UPROPERTY(BlueprintAssignable)
	FOnRightClick evOnRightClick;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<UActorComponent*> weapons;
protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface
	virtual void BeginPlay() override;
	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();
	void OnAimDestinationPressed();
	void OnAimDestinationReleased();
	void OnTouchPressed(const ETouchIndex::Type FingerIndex, const FVector Location);
	void OnTouchReleased(const ETouchIndex::Type FingerIndex, const FVector Location);

	UFUNCTION(BlueprintCallable)
	const ATopDownBase50Character* GetMyPlayerCharacter() const { return Cast<ATopDownBase50Character>(GetPawn());}

private:
	bool bInputPressed; // Input is bring pressed
	bool bIsTouch; // Is it a touch device
	float FollowTime; // For how long it has been pressed
};


