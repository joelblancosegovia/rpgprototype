// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponComponent.h"
#include "TopDownBase50Character.h"
#include "Bullet.h"
#include "TopDownBase50PlayerController.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values for this component's properties
UWeaponComponent::UWeaponComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	// auto controller = Cast<ATopDownBase50PlayerController>(GetWorld()->GetFirstPlayerController());
	// if(controller)
	// {
	// 	controller->evOnRightClick.AddDynamic(this, &UWeaponComponent::Fire);
	// }
	
}

// Called every frame
void UWeaponComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UWeaponComponent::AttachToPlayer(ATopDownBase50Character* target) {

	selfActor = target;
}

void UWeaponComponent::Fire(FVector HitLocation)
{
	if (!selfActor || !selfActor->GetController())
	{
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("pium"));

	UWorld* const w = GetWorld();

	if (!w)
		return;
	
	if(!bulletClass){
		return;
	}
	
	APlayerController* pchar = Cast<APlayerController>(selfActor->GetController());
	const FVector sPos = selfActor->GetActorLocation() + selfActor->GetActorRotation().RotateVector(shootPoint);

	FActorSpawnParameters sParams;
	sParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	w->SpawnActor<ABullet>(bulletClass, sPos, selfActor->GetActorRotation(), sParams);
}

