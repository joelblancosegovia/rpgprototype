// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "DataTableClasses.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum EClases {
	SOLDIER,
	BULLDOZER,
	PSYOPS
};

USTRUCT(BlueprintType)
struct TOPDOWNBASE50_API FDataTableClasses : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<EClases> myclass;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialHp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int moveSpeed;
	
};
