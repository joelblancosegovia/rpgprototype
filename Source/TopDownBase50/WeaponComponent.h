// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TopDownBase50Character.h"
#include "Components/ActorComponent.h"
#include "WeaponComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNBASE50_API UWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWeaponComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	ATopDownBase50Character* selfActor;
	
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Proyectile)
	TSubclassOf<class ABullet> bulletClass;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector shootPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Proyectile)
	float shootSpeed;

public:
	UFUNCTION(BlueprintCallable, Category=Gameplay)
		void AttachToPlayer(ATopDownBase50Character* target);

	UFUNCTION(BlueprintCallable, Category=Gameplay)
		void Fire(FVector HitLocation);
};

