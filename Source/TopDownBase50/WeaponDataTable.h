// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "WeaponDataTable.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum EWeaponClases {
	HANDGUN,
	RIFLE
};

USTRUCT(BlueprintType)
struct TOPDOWNBASE50_API FWeaponDataTable : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<EWeaponClases> myclass;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialDamage;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float initialShotSpeed;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialAmmo;
	
};
