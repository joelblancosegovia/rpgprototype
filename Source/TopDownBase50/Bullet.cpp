// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"

#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
ABullet::ABullet()
{
	PrimaryActorTick.bCanEverTick = true;

	collComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	collComp->InitSphereRadius(10.f);
	RootComponent = collComp;
	collComp->SetCollisionProfileName("BlockAllDynamic");
	collComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	collComp->CanCharacterStepUpOn = ECB_No;
	collComp->SetEnableGravity(false);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> defaultSphere(TEXT("/Game/LevelPrototyping/Meshes/SM_Cube.SM_Cube"));
	bulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("bullet"));
	bulletMesh->SetupAttachment(RootComponent);
	if (defaultSphere.Succeeded())
	{
		bulletMesh->SetStaticMesh(defaultSphere.Object);
		bulletMesh->SetRelativeLocation(FVector(0.f,0.f,30.f));
		bulletMesh->SetRelativeScale3D(FVector(.75f,.75f,.75f));
	}

	
	projMove = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjMovement"));
	projMove->UpdatedComponent = collComp;
	projMove->InitialSpeed = 500.f;
	projMove->MaxSpeed = 500.f;
	projMove->bRotationFollowsVelocity = true;
	projMove->bShouldBounce = false;
	
	InitialLifeSpan = 30.f;

}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	collComp->OnComponentBeginOverlap.AddDynamic(this, &ABullet::OnHit);
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABullet::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit) {
	if(OtherActor && IsValid(OtherActor)) {
		UE_LOG(LogTemp, Warning, TEXT("Golpié %s"), *(OtherActor->GetName()));
		//AEnemy* e = Cast<Enemy>(hit.getActor());
		// e->Hit(x);
		//Destroy();
	}
};


